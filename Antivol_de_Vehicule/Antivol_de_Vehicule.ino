#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;

int button=A0;
int securite =true;
int ledRouge=5;
int ledBleu=6;
int buzzer=7;

void setup() {
 // put your setup code here, to run once:
 pinMode(ledRouge,OUTPUT);
 pinMode(ledBleu,OUTPUT);
 pinMode(buzzer,OUTPUT);
 pinMode(2,INPUT);
 Serial.begin(9600);
 lcd.begin(16, 2);

 digitalWrite(ledBleu,HIGH);
 lcd.print("Securite ON");
 lcd.setRGB(0, 120, 230);
}

void mode() {

   securite = !securite;
   if (securite == false) {
    
   lcd.clear();
   digitalWrite(ledRouge,HIGH);
   digitalWrite(ledBleu,LOW);
   lcd.print("Securite OFF");
   lcd.setRGB(255, 0, 0);
   }
   
   else {
    lcd.clear();
    digitalWrite(ledBleu,HIGH);
    digitalWrite(ledRouge,LOW);
    lcd.print("Securite ON");
    lcd.setRGB(0, 120, 230);

  }
  delay(2000);
  
}


void sensor() {

  int sensorState = digitalRead(2);
  if(sensorState == HIGH)    {
      
    digitalWrite(ledRouge,HIGH);
    digitalWrite(ledBleu,LOW);
    digitalWrite(buzzer,HIGH);
    delay(3000);
        
    }
  else  {
    digitalWrite(ledRouge,LOW);
    digitalWrite(ledBleu,HIGH);
  }
  digitalWrite(buzzer,LOW);
}


void loop() {
 // put your main code here, to run repeatedly:
 
 if (analogRead(button)!=0) {
   mode();
 }
 
 else {
   if (securite == true) {
    sensor();
   }
   

 
 
 }
}
